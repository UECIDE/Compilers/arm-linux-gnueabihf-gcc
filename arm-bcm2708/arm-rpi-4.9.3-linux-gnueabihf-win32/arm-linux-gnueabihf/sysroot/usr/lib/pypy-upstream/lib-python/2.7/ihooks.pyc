	�
�U�Qc              s�  d  Z  d d l m Z m Z e d d d �[ d d l Z d d l Z d d l Z d d l Z d d d	 d
 d d d d g Z d Z	 d d l m
 Z
 m Z m Z d d l m Z m Z m Z e Z e Z d f  d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d	 e f d �  �  YZ d
 e f d �  �  YZ d e f d �  �  YZ d e f d �  �  YZ d Z d a d d � Z d �  Z d S(   s,	  Import hook support.

Consistent use of this module will make it possible to change the
different mechanisms involved in loading modules independently.

While the built-in module imp exports interfaces to the built-in
module searching and loading algorithm, and it is possible to replace
the built-in function __import__ in order to change the semantics of
the import statement, until now it has been difficult to combine the
effect of different __import__ hacks, like loading modules from URLs
by rimport.py, or restricted execution by rexec.py.

This module defines three new concepts:

1) A "file system hooks" class provides an interface to a filesystem.

One hooks class is defined (Hooks), which uses the interface provided
by standard modules os and os.path.  It should be used as the base
class for other hooks classes.

2) A "module loader" class provides an interface to search for a
module in a search path and to load it.  It defines a method which
searches for a module in a single directory; by overriding this method
one can redefine the details of the search.  If the directory is None,
built-in and frozen modules are searched instead.

Two module loader class are defined, both implementing the search
strategy used by the built-in __import__ function: ModuleLoader uses
the imp module's find_module interface, while HookableModuleLoader
uses a file system hooks class to interact with the file system.  Both
use the imp module's load_* interfaces to actually load the module.

3) A "module importer" class provides an interface to import a
module, as well as interfaces to reload and unload a module.  It also
provides interfaces to install and uninstall itself instead of the
default __import__ and reload (and unload) functions.

One module importer class is defined (ModuleImporter), which uses a
module loader instance passed in (by default HookableModuleLoader is
instantiated).

The classes defined here should be used as base classes for extended
functionality along those lines.

If a module importer class supports dotted names, its import_module()
must return a different value depending on whether it is called on
behalf of a "from ... import ..." statement or not.  (This is caused
by the way the __import__ hook is used by the Python interpreter.)  It
would also do wise to install a different version of reload().

i����(   s   warnpy3ks   warns0   the ihooks module has been removed in Python 3.0s
   stackleveli   Ns   BasicModuleLoaders   Hookss   ModuleLoaders   FancyModuleLoaders   BasicModuleImporters   ModuleImporters   installs	   uninstalli    (   s   C_EXTENSIONs	   PY_SOURCEs   PY_COMPILED(   s	   C_BUILTINs	   PY_FROZENs   PKG_DIRECTORYs   _Verbosec              s8   e  Z e d  � Z d �  Z d �  Z d �  Z d �  Z RS(   c         C   s   | |  _  d  S(   N(   t   verbose(   t   selft   verbose(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   __init__K   s    c         C   s   |  j  S(   N(   t   verbose(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   get_verboseN   s    c         C   s   | |  _  d  S(   N(   t   verbose(   t   selft   verbose(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   set_verboseQ   s    c         G   s   |  j  r |  j | �  n  d  S(   N(   t   verboset   message(   t   selft   args(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   noteV   s    	c         G   s   | r | | GHn | GHd  S(   N(    (   t   selft   formatt   args(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   messageZ   s    (   t   __name__t
   __module__t   VERBOSEt   __init__t   get_verboset   set_verboset   notet   message(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   _VerboseI   s
   			c             s>   e  Z d  Z d d � Z d �  Z d �  Z d �  Z d �  Z RS(   s;  Basic module loader.

    This provides the same functionality as built-in import.  It
    doesn't deal with checking sys.modules -- all it provides is
    find_module() and a load_module(), as well as find_module_in_dir()
    which searches just one directory, and can be overridden by a
    derived class to change the module search algorithm when the basic
    dependency on sys.path is unchanged.

    The interface is a little more convenient than imp's:
    find_module(name, [path]) returns None or 'stuff', and
    load_module(name, stuff) loads the module.

    Nc         C   sS   | d  k r" d  g |  �  �  } n  x* | D]" } |  � | | � } | rK | Sq) Wd  S(   N(   t   default_patht   find_module_in_dir(   t   selft   namet   patht   dirt   stuff(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   find_moduler   s      c         C   s   t  j S(   N(   t   syst   path(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   default_pathz   s    c         C   sE   | d  k r |  �  | � Sy t � | | g � St k
 r@ d  SXd  S(   N(   t   find_builtin_modulet   impt   find_modulet   ImportError(   t   selft   namet   dir(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   find_module_in_dir}   s    c         C   sN   t  � | � r% d  d d d t f f St  � | � rJ d  d d d t f f Sd  S(   Ns    (   t   impt
   is_builtint   BUILTIN_MODULEt	   is_frozent   FROZEN_MODULE(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   find_builtin_module�   s
    c         C   s@   | \ } } } z t  � | | | | � S| r; | � �  n  Xd  S(   N(   t   impt   load_modulet   close(   t   selft   namet   stufft   filet   filenamet   info(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_module�   s
     (   t   __name__t
   __module__t   __doc__t   find_modulet   default_patht   find_module_in_dirt   find_builtin_modulet   load_module(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   BasicModuleLoadera   s   				c             s�   e  Z d  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z	 d d	 � Z
 d d
 � Z d d � Z d d � Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z e Z d �  Z e j Z RS(   s�   Hooks into the filesystem and interpreter.

    By deriving a subclass you can redefine your filesystem interface,
    e.g. to merge it with the URL space.

    This base class behaves just like the native filesystem.

    c         C   s
   t  � �  S(   N(   t   impt   get_suffixes(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   get_suffixes�   s    c         C   s   t  � | � S(   N(   t   impt
   new_module(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   new_module�   s    c         C   s   t  � | � S(   N(   t   impt
   is_builtin(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   is_builtin�   s    c         C   s   t  � | � S(   N(   t   impt   init_builtin(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   init_builtin�   s    c         C   s   t  � | � S(   N(   t   impt	   is_frozen(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   is_frozen�   s    c         C   s   t  � | � S(   N(   t   impt   init_frozen(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   init_frozen�   s    c         C   s   t  � | � S(   N(   t   impt   get_frozen_object(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   get_frozen_object�   s    Nc         C   s   t  � | | | � S(   N(   t   impt   load_source(   t   selft   namet   filenamet   file(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_source�   s    c         C   s   t  � | | | � S(   N(   t   impt   load_compiled(   t   selft   namet   filenamet   file(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_compiled�   s    c         C   s   t  � | | | � S(   N(   t   impt   load_dynamic(   t   selft   namet   filenamet   file(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_dynamic�   s    c         C   s   t  � | | | d d t f � S(   Ns    (   t   impt   load_modulet   PKG_DIRECTORY(   t   selft   namet   filenamet   file(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_package�   s    c         C   s;   |  �  �  } | | k r  | | S|  � | � | | <} | S(   N(   t   modules_dictt
   new_module(   t   selft   namet   dt   m(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   add_module�   s
     c         C   s   t  j S(   N(   t   syst   modules(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   modules_dict�   s    c         C   s   t  j S(   N(   t   syst   path(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   default_path�   s    c         C   s   t  j � | � S(   N(   t   ost   patht   split(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   path_split�   s    c         C   s   t  j � | | � S(   N(   t   ost   patht   join(   t   selft   xt   y(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   path_join�   s    c         C   s   t  j � | � S(   N(   t   ost   patht   isabs(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   path_isabs�   s    c         C   s   t  j � | � S(   N(   t   ost   patht   exists(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   path_exists�   s    c         C   s   t  j � | � S(   N(   t   ost   patht   isdir(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   path_isdir�   s    c         C   s   t  j � | � S(   N(   t   ost   patht   isfile(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   path_isfile�   s    c         C   s   t  j � | � S(   N(   t   ost   patht   islink(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   path_islink�   s    c         G   s
   t  | �  S(   N(   t   open(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   openfile�   s    c         C   s   t  � | � S(   N(   t   ost   listdir(   t   selft   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   listdir�   s    (   t   __name__t
   __module__t   __doc__t   get_suffixest
   new_modulet
   is_builtint   init_builtint	   is_frozent   init_frozent   get_frozen_objectt   load_sourcet   load_compiledt   load_dynamict   load_packaget
   add_modulet   modules_dictt   default_patht
   path_splitt	   path_joint
   path_isabst   path_existst
   path_isdirt   path_isfilet   path_islinkt   openfilet   IOErrort   openfile_errort   listdirt   ost   errort   listdir_error(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   Hooks�   s4   
																			c             s_   e  Z d  Z d e d � Z d �  Z d �  Z d �  Z d �  Z d �  Z	 d d	 � Z
 d
 �  Z RS(   s�   Default module loader; uses file system hooks.

    By defining suitable hooks, you might be able to load modules from
    other sources than the file system, e.g. from compressed or
    encrypted files, tar files or (if you're brave!) URLs.

    Nc         C   s)   t  � |  | � | p t | � |  _ d  S(   N(   t   BasicModuleLoadert   __init__t   Hookst   hooks(   t   selft   hookst   verbose(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   __init__�   s    c         C   s   |  j  � �  S(   N(   t   hookst   default_path(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   default_path�   s    c         C   s   |  j  � �  S(   N(   t   hookst   modules_dict(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   modules_dict�   s    c         C   s   |  j  S(   N(   t   hooks(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   get_hooks�   s    c         C   s   | |  _  d  S(   N(   t   hooks(   t   selft   hooks(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   set_hooks�   s    c         C   sT   |  j  � | � r( d  d d d t f f S|  j  � | � rP d  d d d t f f Sd  S(   Ns    (   t   hookst
   is_builtint   BUILTIN_MODULEt	   is_frozent   FROZEN_MODULE(   t   selft   name(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   find_builtin_module�   s
    i   c         C   s  | d  k r |  �  | � S| r� |  j � | | � } |  j � | � r� |  � d | d � } | r� | d } | r~ | � �  n  d  | d d t f f Sn  n  x{ |  j � �  D]j } | \ } }	 }
 |  j � | | | � } y" |  j � | |	 � } | | | f S|  j j	 k
 rq� Xq� Wd  S(   Ns   __init__i    s    (
   t   find_builtin_modulet   hookst	   path_joint
   path_isdirt   find_module_in_dirt   closet   PKG_DIRECTORYt   get_suffixest   openfilet   openfile_error(   t   selft   namet   dirt   allow_packagest   fullnamet   stufft   filet   infot   sufft   modet   typet   fp(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   find_module_in_dir�   s*    
  c   
   	   C   s-  | \ } } } | \ } } } z� | t  k r= |  j � | � S| t k rY |  j � | � S| t k r� |  j � | | | � }	 n� | t k r� |  j � | | | � }	 na | t	 k r� |  j �
 | | | � }	 n: | t k r� |  j � | | | � }	 n t d | | f � Wd  | r| � �  n  X| |	 _ |	 S(   Ns$   Unrecognized module type (%r) for %s(   t   BUILTIN_MODULEt   hookst   init_builtint   FROZEN_MODULEt   init_frozent   C_EXTENSIONt   load_dynamict	   PY_SOURCEt   load_sourcet   PY_COMPILEDt   load_compiledt   PKG_DIRECTORYt   load_packaget   ImportErrort   closet   __file__(
   t   selft   namet   stufft   filet   filenamet   infot   sufft   modet   typet   m(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_module  s*     	(   t   __name__t
   __module__t   __doc__t   VERBOSEt   __init__t   default_patht   modules_dictt	   get_hookst	   set_hookst   find_builtin_modulet   find_module_in_dirt   load_module(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   ModuleLoader�   s   					c             s   e  Z d  Z d �  Z RS(   s8   Fancy module loader -- parses and execs the code itself.c         B   s�  | \ } } \ } } } | } d  }	 | e  k r� |  � d | d � }
 |
 s[ e d | � n  |
 \ } } } | \ } } } | e e f k r� | r� | � �  n  e d | | f � n  | g }	 | } | } | } n  | e k r� |  j � | � } nw | e k r*d d  l	 } | �
 d � | � | � } n@ | e k rW| � �  } e | | d � } n e � |  | | � S|  j � | � } |	 r�|	 | _ n  | | _ y | | j UWn/ |  j � �  } | | k r�| | =n  �  n X| S(   Ns   __init__i    s    No __init__ module in package %ss/   Bad type (%r) for __init__ module in package %si����i   s   exec(   t   PKG_DIRECTORYt   find_module_in_dirt   ImportErrort   PY_COMPILEDt	   PY_SOURCEt   closet   FROZEN_MODULEt   hookst   get_frozen_objectt   marshalt   seekt   loadt   readt   compilet   ModuleLoadert   load_modulet
   add_modulet   __path__t   __file__t   __dict__t   modules_dict(   t   selft   namet   stufft   filet   filenamet   sufft   modet   typet   realfilenamet   patht	   initstufft   initfilet   initfilenamet   initinfot   initsufft   initmodet   inittypet   codet   marshalt   datat   mt   d(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   load_module"  sR     			
(   t   __name__t
   __module__t   __doc__t   load_module(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   FancyModuleLoader  s   c             sz   e  Z d  Z d e d � Z d �  Z d �  Z d �  Z d �  Z i  i  g  d � Z	 d d � Z
 d	 �  Z d
 �  Z d �  Z RS(   ss   Basic module importer; uses module loader.

    This provides basic import facilities but no package imports.

    Nc         C   s>   t  � |  | � | p" t d  | � |  _ |  j � �  |  _ d  S(   N(   t   _Verboset   __init__t   ModuleLoadert   loadert   modules_dictt   modules(   t   selft   loadert   verbose(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   __init__Y  s    c         C   s   |  j  S(   N(   t   loader(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   get_loader^  s    c         C   s   | |  _  d  S(   N(   t   loader(   t   selft   loader(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt
   set_loadera  s    c         C   s   |  j  � �  S(   N(   t   loadert	   get_hooks(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   get_hooksd  s    c         C   s   |  j  � | � S(   N(   t   loadert	   set_hooks(   t   selft   hooks(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   set_hooksg  s    c         C   sa   t  | � } | |  j k r& |  j | S|  j � | � } | sN t d | � n  |  j � | | � S(   Ns   No module named %s(   t   strt   modulest   loadert   find_modulet   ImportErrort   load_module(   t   selft   namet   globalst   localst   fromlistt   stuff(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   import_modulej  s    c         C   sM   t  | j � } |  j � | | � } | s: t d | � n  |  j � | | � S(   Ns   Module %s not found for reload(   t   strt   __name__t   loadert   find_modulet   ImportErrort   load_module(   t   selft   modulet   patht   namet   stuff(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   reloads  s
    c         C   s   |  j  t | j � =d  S(   N(   t   modulest   strt   __name__(   t   selft   module(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   unloadz  s    c         C   sg   t  j |  _ t  j |  _ t t  d � s3 d  t  _ n  t  j |  _ |  j t  _ |  j t  _ |  j t  _ d  S(   Ns   unload(	   t   __builtin__t
   __import__t   save_import_modulet   reloadt   save_reloadt   hasattrt   unloadt   save_unloadt   import_module(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   install~  s    c         C   s:   |  j  t _ |  j t _ |  j t _ t j s6 t ` n  d  S(   N(   t   save_import_modulet   __builtin__t
   __import__t   save_reloadt   reloadt   save_unloadt   unload(   t   self(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   uninstall�  s
    	(   t   __name__t
   __module__t   __doc__t   VERBOSEt   __init__t
   get_loadert
   set_loadert	   get_hookst	   set_hookst   import_modulet   reloadt   unloadt   installt	   uninstall(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   BasicModuleImporterQ  s   							
c             sb   e  Z d  Z d d d d d � Z d d � Z d �  Z d �  Z d d � Z d d	 � Z d
 �  Z	 RS(   s)   A module importer that supports packages.Ni����c   
      C   sr   |  �  | | � } |  � | t | � � \ } } |  � | | � }	 | sL | St |	 d � rn |  � |	 | � n  |	 S(   Ns   __path__(   t   determine_parentt   find_head_packaget   strt	   load_tailt   hasattrt   ensure_fromlist(
   t   selft   namet   globalst   localst   fromlistt   levelt   parentt   qt   tailt   m(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   import_module�  s    c         C   s�  | r | r n d  S| �  d � } | d  k	 rO | sL | d k rL t d � n  n� | �  d � } | d  k rn d  Sd | k r� | } nE d | k r� | d k r� t d � n  d  | d <d  S| � d � d } | | d <| d k rMt | � } xS t | d d � D]? } y | � d d | � } Wn t k
 r;t d	 � � q� Xq� W| |  } n  y t j | St k
 r�| d k  r�t	 d
 | t
 d � d  St d | � n Xd  S(   Ns   __package__i    s(   Attempted relative import in non-packages   __name__s   __path__s   .i   i����s2   attempted relative import beyond top-level packages;   Parent module '%s' not found while handling absolute imports=   Parent module '%s' not loaded, cannot perform relative import(   t   gett
   ValueErrort
   rpartitiont   lent   ranget   rindext   syst   modulest   KeyErrort   warnt   RuntimeWarningt   SystemError(   t   selft   globalst   levelt   pkgnamet   modnamet   dott   x(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   determine_parent�  sH    	

 c         C   s�   d | k r6 | �  d � } | |  } | | d } n | } d } | r^ d | j | f } n | } |  � | | | � } | r� | | f S| r� | } d  } |  � | | | � } | r� | | f Sn  t d | � d  S(   Ns   .i   s    s   %s.%ss   No module named '%s'(   t   findt   __name__t	   import_itt   ImportError(   t   selft   parentt   namet   it   headt   tailt   qnamet   q(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   find_head_package�  s&    
 
 c         C   s�   | } x� | r� | �  d � } | d k  r9 t | � } n  | |  | | d } } d | j | f } |  � | | | � } | s� t d | � q	 q	 W| S(   Ns   .i    i   s   %s.%ss   No module named '%s'(   t   findt   lent   __name__t	   import_itt   ImportError(   t   selft   qt   tailt   mt   it   headt   mname(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   load_tail�  s      i    c         C   s�   x� | D]� } | d k r\ | sV y | j  } Wn t k
 r? n X|  � | | d � q q n  | d k r� t | | � s� d | j | f } |  � | | | � } | s� t d | � n  q q Wd  S(   Ns   *i   s   %s.%ss   No module named '%s'(   t   __all__t   AttributeErrort   ensure_fromlistt   hasattrt   __name__t	   import_itt   ImportError(   t   selft   mt   fromlistt	   recursivet   subt   allt   subnamet   submod(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   ensure_fromlist�  s     c         C   s�   | s
 | S| s2 y |  j  | St k
 r. n Xn  y | oA | j } Wn t k
 rY d  SXt | � } |  j � | | � } | s� d  St | � } |  j � | | � } | r� t | | | � n  | S(   N(	   t   modulest   KeyErrort   __path__t   AttributeErrort   strt   loadert   find_modulet   load_modulet   setattr(   t   selft   partnamet   fqnamet   parentt
   force_loadt   patht   stufft   m(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   import_it�  s(    c      	   C   s{   t  | j � } d | k r4 |  � | | d  d d �S| � d � } | |  } |  j | } |  � | | d | | d d �S(   Ns   .s
   force_loadi   (   t   strt   __name__t	   import_itt   rfindt   modules(   t   selft   modulet   namet   it   pnamet   parent(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   reload  s    
(
   t   __name__t
   __module__t   __doc__t   import_modulet   determine_parentt   find_head_packaget	   load_tailt   ensure_fromlistt	   import_itt   reload(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   ModuleImporter�  s   	
-		c         C   s#   |  p t  p t �  a t � �  d  S(   N(   t   default_importert   ModuleImportert   current_importert   install(   t   importer(    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   install#  s    c           C   s   t  � �  d  S(   N(   t   current_importert	   uninstall(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt	   uninstall(  s    (   t   __doc__t   warningst   warnpy3kt   warnt   __builtin__t   impt   ost   syst   __all__t   VERBOSEt   C_EXTENSIONt	   PY_SOURCEt   PY_COMPILEDt	   C_BUILTINt	   PY_FROZENt   PKG_DIRECTORYt   BUILTIN_MODULEt   FROZEN_MODULEt   _Verboset   BasicModuleLoadert   Hookst   ModuleLoadert   FancyModuleLoadert   BasicModuleImportert   ModuleImportert   default_importert   current_importert   installt	   uninstall(    (    (    t/   /usr/lib/pypy-upstream/lib-python/2.7/ihooks.pyt   <module>   s2   358P3?�